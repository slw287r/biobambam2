/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <config.h>

#include <libmaus2/vcf/VCFParser.hpp>
#include <libmaus2/vcf/VCFSortEntry.hpp>
#include <libmaus2/vcf/VCFSorter.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/lz/BgzfDeflate.hpp>

#include <biobambam2/Licensing.hpp>

#include <libmaus2/util/AutoArrayCharBaseAllocator.hpp>
#include <libmaus2/util/AutoArrayCharBaseTypeInfo.hpp>
#include <libmaus2/util/GrowingFreeList.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>

// static int getDefaultVerbose() { return 0; }
static int getDefaultGZ() { return 0; }
static int getDefaultDedup() { return 0; }

int vcfsort(libmaus2::util::ArgInfo const & arginfo, std::istream & in, std::ostream & out)
{
	bool const gz = arginfo.getValue<int>("gz",getDefaultGZ());
	int const dedup = arginfo.getValue<int>("dedup",getDefaultDedup());
	std::string const tmpfilename = arginfo.getUnparsedValue("T",arginfo.getDefaultTmpFileName());

	libmaus2::vcf::VCFSorter::sort(in,out,gz,dedup,tmpfilename);

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				std::cerr << std::endl;
				std::cerr << "Key=Value pairs:" << std::endl;
				std::cerr << std::endl;

				std::vector< std::pair<std::string,std::string> > V;

				V.push_back ( std::pair<std::string,std::string> ( "gz=<["+::biobambam2::Licensing::formatNumber(getDefaultGZ())+"]>", "compress output (default: 0)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "dedup=<["+::biobambam2::Licensing::formatNumber(getDefaultDedup())+"]>", "remove duplicates by checking columns 1,2,4 and 5 for identity (default: 0)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "T=<["+arginfo.getDefaultTmpFileName()+"]>", "temporary file name used (default: use temp file in current directory)" ) );

				::biobambam2::Licensing::printMap(std::cerr,V);

				std::cerr << std::endl;
				return EXIT_SUCCESS;
			}

		return vcfsort(arginfo,std::cin,std::cout);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
