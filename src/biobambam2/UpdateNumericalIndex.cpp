/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <biobambam2/UpdateNumericalIndex.hpp>

#include <libmaus2/bambam/BamNumericalIndexGenerator.hpp>
#include <libmaus2/bambam/BamNumericalIndexDecoder.hpp>

namespace biobambam2
{
	void updateNumericalIndex(std::string const & bamfn, std::pair<uint64_t,std::string> const & compdata)
	{
		std::string const indexfn = libmaus2::bambam::BamNumericalIndexBase::getIndexName(bamfn);

		if ( libmaus2::util::GetFileSize::fileExists(indexfn) )
		{
			// get index stats
			uint64_t alcnt, mod, numblocks;
			{
				libmaus2::bambam::BamNumericalIndexDecoder indexdec(indexfn);
				mod = indexdec.getBlockSize();
				alcnt = indexdec.getAlignmentCount();
				numblocks = indexdec.size();
			}

			// replace values for recompressed part
			std::istringstream compistr(compdata.second + libmaus2::lz::BgzfDeflateBase::getEOFBlock());
			libmaus2::bambam::BamRawDecoderNoThrowEOF BRD(compistr);

			typedef libmaus2::bambam::BamRawDecoderBase::RawInterval raw_interval;

			std::pair<
				std::pair<uint8_t const *,uint64_t>,
				raw_interval
			> P;

			int64_t highestSet = -1;
			for ( uint64_t i = 0; (P = BRD.getPos()).first.first; ++i )
				if ( (i % mod) == 0 )
				{
					assert ( i < alcnt );

					uint64_t const blockid = i / mod;
					std::pair<uint64_t,uint64_t> const & start = P.second.start;

					// std::cerr << "replacing index position for " << i << " by " << start.first << "," << start.second << std::endl;

					libmaus2::bambam::BamNumericalIndexGenerator::replaceValue(
						indexfn,
						blockid,
						start.first,
						start.second
					);

					highestSet = blockid;
				}

			// std::cerr << "shifting index positions for [" << highestSet+1 << "," << numblocks << ")" << " by " << static_cast<int64_t>(compdata.second.size()) - static_cast<int64_t>(compdata.first) << std::endl;

			// shift values in part we moved as is
			libmaus2::bambam::BamNumericalIndexGenerator::shiftValues(
				indexfn,
				highestSet + 1,numblocks,
				static_cast<int64_t>(compdata.second.size()) - static_cast<int64_t>(compdata.first)
			);
		}
	}
}
