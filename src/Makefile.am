ACLOCAL_AMFLAGS=-I m4

noinst_HEADERS=biobambam2/BamBamConfig.hpp biobambam2/Licensing.hpp \
	biobambam2/ClipAdapters.hpp biobambam2/AttachRank.hpp biobambam2/ResetAlignment.hpp \
	biobambam2/Split12.hpp biobambam2/Strip12.hpp \
	biobambam2/ClipReinsert.hpp biobambam2/zzToName.hpp \
	biobambam2/KmerPoisson.hpp \
	biobambam2/RgInfo.hpp \
	biobambam2/ReadHeader.hpp \
	biobambam2/RunEOFFilter.hpp \
	biobambam2/UpdateNumericalIndex.hpp \
	biobambam2/DepthInterval.hpp \
	biobambam2/DepthIntervalGetter.hpp

MANPAGES = programs/bamtofastq.1 programs/bamsort.1 programs/bammarkduplicates.1 programs/bammarkduplicatesopt.1 \
	programs/bammaskflags.1 programs/bamrecompress.1 programs/bamadapterfind.1 \
	programs/bamfilteraux.1 programs/bamauxsort.1 programs/bamadapterclip.1 \
	programs/bamcollate2.1 programs/bam12auxmerge.1 programs/bam12split.1 \
	programs/bam12strip.1 programs/bamreset.1 programs/bamrank.1 \
	programs/bamclipreinsert.1 programs/bamzztoname.1 \
	programs/bamindex.1 programs/bamfilterflags.1 \
	programs/bamfilterrg.1 programs/bamdownsamplerandom.1 \
	programs/bammarkduplicates2.1 \
	programs/bamcat.1 programs/bammerge.1 programs/bamsplit.1 \
	programs/bamsplitdiv.1 programs/bamchecksort.1 programs/fastqtobam.1 \
	programs/bamfixmateinformation.1 programs/normalisefasta.1 \
	programs/bamfilterheader.1 \
	programs/bamfilterheader2.1 \
	programs/bammdnm.1 \
	programs/bamseqchksum.1 programs/bamvalidate.1 \
	programs/bamflagsplit.1 programs/bamintervalcomment.1 \
	programs/bamstreamingmarkduplicates.1 \
	programs/bamsormadup.1 programs/fastaexplod.1 \
	programs/bamrecalculatecigar.1 programs/bamfiltermc.1 \
	programs/bamfillquery.1 programs/bamalignfrac.1 programs/bamauxmerge.1 programs/bamauxmerge2.1 \
	programs/bambisect.1 programs/bamclipXT.1 programs/bamconsensus.1 \
	programs/filtergtf.1 programs/bamfeaturecount.1

man_MANS = ${MANPAGES}

EXTRA_DIST = ${MANPAGES}

bin_PROGRAMS = \
	bamtofastq \
	bammarkduplicates \
	bamsort \
	bammaskflags \
	bamrecompress \
	bamadapterfind \
	bamfilteraux \
	bamauxsort \
	bamadapterclip \
	bamcollate2 \
	bam12auxmerge \
	bam12split \
	bam12strip \
	bamreset \
	bamrank \
	bamclipreinsert \
	bamclipXT \
	bamzztoname \
	bamindex \
	bamfilterflags \
	bamfilterrg \
	bamdownsamplerandom \
	bammarkduplicates2 \
	bamcat \
	bammerge \
	bamsplit \
	bamfastsplit \
	bamsplitdiv \
	bamchecksort \
	fastqtobam \
	bamfixmateinformation \
	bamseqchksum \
	normalisefasta \
	bamfilterheader \
	bamfilterheader2 \
	bammdnm \
	bammapdist \
	bamvalidate \
	bamflagsplit \
	bamintervalcomment \
	bamintervalcommenthist \
	bamstreamingmarkduplicates \
	bamalignfrac \
	bamfilternames \
	bamsormadup \
	bamexplode \
	bamexploderef \
	bamfastexploderef \
	bamfastnumextract \
	bamranksort \
	fastaexplod \
	bamrecalculatecigar \
	bamfiltermc \
	bamnumericalindex \
	bamnumericalindexstats \
	bammarkduplicatesopt \
	bamfilterlength \
	bamauxmerge \
	bamrefinterval \
	bamscrapcount \
	bamfilterrefid \
	bamfillquery \
	bamrefextract \
	fastqtobam2 \
	bamauxmerge2 \
	bamreheader \
	bamreplacechecksums \
	bamfastcat \
	populaterefcache \
	bamfiltereofblocks \
	bamdepth \
	bamdepthintersect \
	bamfilterk \
	bamfixpairinfo \
	bamfeaturecount @BLASTXMLTOBAMINSTEXP@ @UNCOMMONINSTALLED@

noinst_PROGRAMS = @BAMREFDEPTHPEAKS@ @BLASTXMLTOBAMNOINSTEXP@ @UNCOMMONUNINSTALLED@
	
EXTRA_PROGRAMS = blastnxmltobam \
	bamfilter \
	bamfilterbyname \
	bamfixmatecoordinates \
	bamfixmatecoordinatesnamesorted \
	bamtoname \
	bamdisthist \
	fastabgzfextract \
	bamheap \
	bamfrontback \
	bamrandomtag \
	bamheap2 \
	bamheap3 \
	bamtagconversion \
	fastqtobampar \
	bambisect \
	bamconsensus \
	vcffilterinfo \
	vcffiltersamples \
	vcfpatchcontigprepend \
	vcfconcat \
	vcfsort \
	vcfreplacecontigs \
	filtergtf \
	bamexploderg \
	bamexondepth \
	bamheadercat \
	bammarkduplicatesoptdist \
	vcfdiff \
	bamsimpledepth \
	bamdepthmerge \
	bamcountflags

populaterefcache_SOURCES = programs/populaterefcache.cpp biobambam2/Licensing.cpp
populaterefcache_LDADD = ${LIBMAUS2LIBS}
populaterefcache_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
populaterefcache_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamreheader_SOURCES = programs/bamreheader.cpp biobambam2/Licensing.cpp biobambam2/ReadHeader.cpp biobambam2/RunEOFFilter.cpp biobambam2/UpdateNumericalIndex.cpp
bamreheader_LDADD = ${LIBMAUS2LIBS}
bamreheader_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamreheader_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfastcat_SOURCES = programs/bamfastcat.cpp biobambam2/Licensing.cpp biobambam2/ReadHeader.cpp biobambam2/RunEOFFilter.cpp
bamfastcat_LDADD = ${LIBMAUS2LIBS}
bamfastcat_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfastcat_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcffilterinfo_SOURCES = programs/vcffilterinfo.cpp biobambam2/Licensing.cpp
vcffilterinfo_LDADD = ${LIBMAUS2LIBS}
vcffilterinfo_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcffilterinfo_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcffiltersamples_SOURCES = programs/vcffiltersamples.cpp biobambam2/Licensing.cpp
vcffiltersamples_LDADD = ${LIBMAUS2LIBS}
vcffiltersamples_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcffiltersamples_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcfpatchcontigprepend_SOURCES = programs/vcfpatchcontigprepend.cpp biobambam2/Licensing.cpp
vcfpatchcontigprepend_LDADD = ${LIBMAUS2LIBS}
vcfpatchcontigprepend_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcfpatchcontigprepend_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamconsensus_SOURCES = programs/bamconsensus.cpp biobambam2/Licensing.cpp
bamconsensus_LDADD = ${LIBMAUS2LIBS}
bamconsensus_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamconsensus_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

fastqtobam2_SOURCES = programs/fastqtobam2.cpp biobambam2/Licensing.cpp biobambam2/KmerPoisson.cpp biobambam2/RunEOFFilter.cpp
fastqtobam2_LDADD = ${LIBMAUS2LIBS}
fastqtobam2_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
fastqtobam2_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamauxmerge2_SOURCES = programs/bamauxmerge2.cpp biobambam2/Licensing.cpp biobambam2/KmerPoisson.cpp
bamauxmerge2_LDADD = ${LIBMAUS2LIBS}
bamauxmerge2_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamauxmerge2_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilter_SOURCES = programs/bamfilter.cpp biobambam2/Licensing.cpp
bamfilter_LDADD = ${LIBMAUS2LIBS}
bamfilter_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilter_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterbyname_SOURCES = programs/bamfilterbyname.cpp biobambam2/Licensing.cpp
bamfilterbyname_LDADD = ${LIBMAUS2LIBS}
bamfilterbyname_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterbyname_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamdepth_SOURCES = programs/bamdepth.cpp biobambam2/Licensing.cpp biobambam2/DepthInterval.cpp
bamdepth_LDADD = ${LIBMAUS2LIBS}
bamdepth_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamdepth_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamsimpledepth_SOURCES = programs/bamsimpledepth.cpp biobambam2/Licensing.cpp biobambam2/DepthInterval.cpp
bamsimpledepth_LDADD = ${LIBMAUS2LIBS}
bamsimpledepth_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamsimpledepth_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamdepthintersect_SOURCES = programs/bamdepthintersect.cpp biobambam2/Licensing.cpp biobambam2/DepthInterval.cpp
bamdepthintersect_LDADD = ${LIBMAUS2LIBS}
bamdepthintersect_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamdepthintersect_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfixmatecoordinates_SOURCES = programs/bamfixmatecoordinates.cpp biobambam2/Licensing.cpp
bamfixmatecoordinates_LDADD = ${LIBMAUS2LIBS}
bamfixmatecoordinates_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfixmatecoordinates_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfixmatecoordinatesnamesorted_SOURCES = programs/bamfixmatecoordinatesnamesorted.cpp biobambam2/Licensing.cpp
bamfixmatecoordinatesnamesorted_LDADD = ${LIBMAUS2LIBS}
bamfixmatecoordinatesnamesorted_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfixmatecoordinatesnamesorted_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammarkduplicates_SOURCES = programs/bammarkduplicates.cpp biobambam2/Licensing.cpp
bammarkduplicates_LDADD = ${LIBMAUS2LIBS}
bammarkduplicates_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammarkduplicates_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamstreamingmarkduplicates_SOURCES = programs/bamstreamingmarkduplicates.cpp biobambam2/Licensing.cpp
bamstreamingmarkduplicates_LDADD = ${LIBMAUS2LIBS}
bamstreamingmarkduplicates_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamstreamingmarkduplicates_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammarkduplicates2_SOURCES = programs/bammarkduplicates2.cpp biobambam2/Licensing.cpp
bammarkduplicates2_LDADD = ${LIBMAUS2LIBS}
bammarkduplicates2_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammarkduplicates2_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammaskflags_SOURCES = programs/bammaskflags.cpp biobambam2/Licensing.cpp
bammaskflags_LDADD = ${LIBMAUS2LIBS}
bammaskflags_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammaskflags_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamsort_SOURCES = programs/bamsort.cpp biobambam2/Licensing.cpp
bamsort_LDADD = ${LIBMAUS2LIBS}
bamsort_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamsort_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamtofastq_SOURCES = programs/bamtofastq.cpp biobambam2/Licensing.cpp
bamtofastq_LDADD = ${LIBMAUS2LIBS}
bamtofastq_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS}
bamtofastq_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamtoname_SOURCES = programs/bamtoname.cpp biobambam2/Licensing.cpp
bamtoname_LDADD = ${LIBMAUS2LIBS}
bamtoname_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS}
bamtoname_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamdisthist_SOURCES = programs/bamdisthist.cpp biobambam2/Licensing.cpp
bamdisthist_LDADD = ${LIBMAUS2LIBS}
bamdisthist_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS}
bamdisthist_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamrecompress_SOURCES = programs/bamrecompress.cpp biobambam2/Licensing.cpp
bamrecompress_LDADD = ${LIBMAUS2LIBS}
bamrecompress_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamrecompress_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamadapterfind_SOURCES = programs/bamadapterfind.cpp biobambam2/Licensing.cpp \
	biobambam2/ClipAdapters.cpp biobambam2/KmerPoisson.cpp
bamadapterfind_LDADD = ${LIBMAUS2LIBS}
bamadapterfind_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamadapterfind_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilteraux_SOURCES = programs/bamfilteraux.cpp biobambam2/Licensing.cpp
bamfilteraux_LDADD = ${LIBMAUS2LIBS}
bamfilteraux_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilteraux_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamauxsort_SOURCES = programs/bamauxsort.cpp biobambam2/Licensing.cpp
bamauxsort_LDADD = ${LIBMAUS2LIBS}
bamauxsort_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamauxsort_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamadapterclip_SOURCES = programs/bamadapterclip.cpp biobambam2/Licensing.cpp \
	biobambam2/ClipAdapters.cpp
bamadapterclip_LDADD = ${LIBMAUS2LIBS}
bamadapterclip_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamadapterclip_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamcollate2_SOURCES = programs/bamcollate2.cpp biobambam2/Licensing.cpp biobambam2/AttachRank.cpp \
	biobambam2/ResetAlignment.cpp
bamcollate2_LDADD = ${LIBMAUS2LIBS}
bamcollate2_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamcollate2_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bam12auxmerge_SOURCES = programs/bam12auxmerge.cpp biobambam2/Licensing.cpp biobambam2/Split12.cpp \
	biobambam2/Strip12.cpp biobambam2/ClipReinsert.cpp biobambam2/zzToName.cpp
bam12auxmerge_LDADD = ${LIBMAUS2LIBS}
bam12auxmerge_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bam12auxmerge_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bam12split_SOURCES = programs/bam12split.cpp biobambam2/Licensing.cpp biobambam2/Split12.cpp
bam12split_LDADD = ${LIBMAUS2LIBS}
bam12split_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bam12split_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bam12strip_SOURCES = programs/bam12strip.cpp biobambam2/Licensing.cpp biobambam2/Strip12.cpp
bam12strip_LDADD = ${LIBMAUS2LIBS}
bam12strip_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bam12strip_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamreset_SOURCES = programs/bamreset.cpp biobambam2/Licensing.cpp biobambam2/ResetAlignment.cpp
bamreset_LDADD = ${LIBMAUS2LIBS}
bamreset_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamreset_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamrank_SOURCES = programs/bamrank.cpp biobambam2/Licensing.cpp biobambam2/AttachRank.cpp
bamrank_LDADD = ${LIBMAUS2LIBS}
bamrank_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamrank_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfillquery_SOURCES = programs/bamfillquery.cpp biobambam2/Licensing.cpp biobambam2/AttachRank.cpp
bamfillquery_LDADD = ${LIBMAUS2LIBS}
bamfillquery_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfillquery_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamclipreinsert_SOURCES = programs/bamclipreinsert.cpp biobambam2/Licensing.cpp \
	biobambam2/ClipReinsert.cpp
bamclipreinsert_LDADD = ${LIBMAUS2LIBS}
bamclipreinsert_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamclipreinsert_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamclipXT_SOURCES = programs/bamclipXT.cpp biobambam2/Licensing.cpp
bamclipXT_LDADD = ${LIBMAUS2LIBS}
bamclipXT_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamclipXT_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamzztoname_SOURCES = programs/bamzztoname.cpp biobambam2/Licensing.cpp biobambam2/zzToName.cpp
bamzztoname_LDADD = ${LIBMAUS2LIBS}
bamzztoname_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamzztoname_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamindex_SOURCES = programs/bamindex.cpp biobambam2/Licensing.cpp
bamindex_LDADD = ${LIBMAUS2LIBS}
bamindex_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamindex_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterflags_SOURCES = programs/bamfilterflags.cpp biobambam2/Licensing.cpp
bamfilterflags_LDADD = ${LIBMAUS2LIBS}
bamfilterflags_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterflags_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterrg_SOURCES = programs/bamfilterrg.cpp biobambam2/Licensing.cpp
bamfilterrg_LDADD = ${LIBMAUS2LIBS}
bamfilterrg_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterrg_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamdownsamplerandom_SOURCES = programs/bamdownsamplerandom.cpp biobambam2/Licensing.cpp
bamdownsamplerandom_LDADD = ${LIBMAUS2LIBS}
bamdownsamplerandom_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamdownsamplerandom_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamcat_SOURCES = programs/bamcat.cpp biobambam2/Licensing.cpp
bamcat_LDADD = ${LIBMAUS2LIBS}
bamcat_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamcat_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammerge_SOURCES = programs/bammerge.cpp biobambam2/Licensing.cpp
bammerge_LDADD = ${LIBMAUS2LIBS}
bammerge_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammerge_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamsplit_SOURCES = programs/bamsplit.cpp biobambam2/Licensing.cpp
bamsplit_LDADD = ${LIBMAUS2LIBS}
bamsplit_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamsplit_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfastsplit_SOURCES = programs/bamfastsplit.cpp biobambam2/Licensing.cpp
bamfastsplit_LDADD = ${LIBMAUS2LIBS}
bamfastsplit_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfastsplit_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamsplitdiv_SOURCES = programs/bamsplitdiv.cpp biobambam2/Licensing.cpp
bamsplitdiv_LDADD = ${LIBMAUS2LIBS}
bamsplitdiv_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamsplitdiv_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamchecksort_SOURCES = programs/bamchecksort.cpp biobambam2/Licensing.cpp
bamchecksort_LDADD = ${LIBMAUS2LIBS}
bamchecksort_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamchecksort_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

fastqtobam_SOURCES = programs/fastqtobam.cpp biobambam2/Licensing.cpp
fastqtobam_LDADD = ${LIBMAUS2LIBS}
fastqtobam_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
fastqtobam_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfixmateinformation_SOURCES = programs/bamfixmateinformation.cpp biobambam2/Licensing.cpp
bamfixmateinformation_LDADD = ${LIBMAUS2LIBS}
bamfixmateinformation_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfixmateinformation_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamseqchksum_SOURCES = programs/bamseqchksum.cpp biobambam2/Licensing.cpp
bamseqchksum_LDADD = ${LIBMAUS2LIBS} @GMPLIBS@
bamseqchksum_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} @GMPLDFLAGS@ ${AM_LDFLAGS}
bamseqchksum_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} @GMPCPPFLAGS@

normalisefasta_SOURCES = programs/normalisefasta.cpp biobambam2/Licensing.cpp
normalisefasta_LDADD = ${LIBMAUS2LIBS}
normalisefasta_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
normalisefasta_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterheader_SOURCES = programs/bamfilterheader.cpp biobambam2/Licensing.cpp
bamfilterheader_LDADD = ${LIBMAUS2LIBS}
bamfilterheader_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterheader_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterheader2_SOURCES = programs/bamfilterheader2.cpp biobambam2/Licensing.cpp
bamfilterheader2_LDADD = ${LIBMAUS2LIBS}
bamfilterheader2_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterheader2_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammdnm_SOURCES = programs/bammdnm.cpp biobambam2/Licensing.cpp
bammdnm_LDADD = ${LIBMAUS2LIBS}
bammdnm_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammdnm_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

fastabgzfextract_SOURCES = programs/fastabgzfextract.cpp biobambam2/Licensing.cpp
fastabgzfextract_LDADD = ${LIBMAUS2LIBS}
fastabgzfextract_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
fastabgzfextract_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilternames_SOURCES = programs/bamfilternames.cpp biobambam2/Licensing.cpp
bamfilternames_LDADD = ${LIBMAUS2LIBS}
bamfilternames_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilternames_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammapdist_SOURCES = programs/bammapdist.cpp biobambam2/Licensing.cpp
bammapdist_LDADD = ${LIBMAUS2LIBS}
bammapdist_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammapdist_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamheap_SOURCES = programs/bamheap.cpp biobambam2/Licensing.cpp
bamheap_LDADD = ${LIBMAUS2LIBS}
bamheap_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamheap_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfrontback_SOURCES = programs/bamfrontback.cpp biobambam2/Licensing.cpp biobambam2/ResetAlignment.cpp
bamfrontback_LDADD = ${LIBMAUS2LIBS}
bamfrontback_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfrontback_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

blastnxmltobam_SOURCES = programs/blastnxmltobam.cpp biobambam2/Licensing.cpp
blastnxmltobam_LDADD = ${LIBMAUS2LIBS} @xerces_c_LIBS@
blastnxmltobam_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
blastnxmltobam_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} @xerces_c_CFLAGS@

bamvalidate_SOURCES = programs/bamvalidate.cpp biobambam2/Licensing.cpp
bamvalidate_LDADD = ${LIBMAUS2LIBS}
bamvalidate_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamvalidate_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamflagsplit_SOURCES = programs/bamflagsplit.cpp biobambam2/Licensing.cpp
bamflagsplit_LDADD = ${LIBMAUS2LIBS}
bamflagsplit_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamflagsplit_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamintervalcomment_SOURCES = programs/bamintervalcomment.cpp biobambam2/Licensing.cpp
bamintervalcomment_LDADD = ${LIBMAUS2LIBS}
bamintervalcomment_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamintervalcomment_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamintervalcommenthist_SOURCES = programs/bamintervalcommenthist.cpp biobambam2/Licensing.cpp
bamintervalcommenthist_LDADD = ${LIBMAUS2LIBS}
bamintervalcommenthist_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamintervalcommenthist_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamrandomtag_SOURCES = programs/bamrandomtag.cpp biobambam2/Licensing.cpp
bamrandomtag_LDADD = ${LIBMAUS2LIBS}
bamrandomtag_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamrandomtag_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamheap2_SOURCES = programs/bamheap2.cpp biobambam2/Licensing.cpp
bamheap2_LDADD = ${LIBMAUS2LIBS}
bamheap2_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamheap2_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamheap3_SOURCES = programs/bamheap3.cpp biobambam2/Licensing.cpp
bamheap3_LDADD = ${LIBMAUS2LIBS}
bamheap3_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamheap3_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamalignfrac_SOURCES = programs/bamalignfrac.cpp biobambam2/Licensing.cpp
bamalignfrac_LDADD = ${LIBMAUS2LIBS}
bamalignfrac_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamalignfrac_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamsormadup_SOURCES = programs/bamsormadup.cpp biobambam2/Licensing.cpp
bamsormadup_LDADD = ${LIBMAUS2LIBS}
bamsormadup_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamsormadup_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamexplode_SOURCES = programs/bamexplode.cpp biobambam2/Licensing.cpp
bamexplode_LDADD = ${LIBMAUS2LIBS}
bamexplode_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamexplode_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamexploderef_SOURCES = programs/bamexploderef.cpp biobambam2/Licensing.cpp
bamexploderef_LDADD = ${LIBMAUS2LIBS}
bamexploderef_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamexploderef_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfastexploderef_SOURCES = programs/bamfastexploderef.cpp biobambam2/Licensing.cpp
bamfastexploderef_LDADD = ${LIBMAUS2LIBS}
bamfastexploderef_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfastexploderef_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfastnumextract_SOURCES = programs/bamfastnumextract.cpp biobambam2/Licensing.cpp
bamfastnumextract_LDADD = ${LIBMAUS2LIBS}
bamfastnumextract_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfastnumextract_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

fastqtobampar_SOURCES = programs/fastqtobampar.cpp biobambam2/Licensing.cpp
fastqtobampar_LDADD = ${LIBMAUS2LIBS}
fastqtobampar_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
fastqtobampar_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamranksort_SOURCES = programs/bamranksort.cpp biobambam2/Licensing.cpp
bamranksort_LDADD = ${LIBMAUS2LIBS}
bamranksort_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamranksort_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamtagconversion_SOURCES = programs/bamtagconversion.cpp biobambam2/Licensing.cpp
bamtagconversion_LDADD = ${LIBMAUS2LIBS}
bamtagconversion_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamtagconversion_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

fastaexplod_SOURCES = programs/fastaexplod.cpp biobambam2/Licensing.cpp
fastaexplod_LDADD = ${LIBMAUS2LIBS}
fastaexplod_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
fastaexplod_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamrecalculatecigar_SOURCES = programs/bamrecalculatecigar.cpp biobambam2/Licensing.cpp
bamrecalculatecigar_LDADD = ${LIBMAUS2LIBS}
bamrecalculatecigar_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamrecalculatecigar_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfiltermc_SOURCES = programs/bamfiltermc.cpp biobambam2/Licensing.cpp
bamfiltermc_LDADD = ${LIBMAUS2LIBS}
bamfiltermc_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfiltermc_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamnumericalindex_SOURCES = programs/bamnumericalindex.cpp biobambam2/Licensing.cpp
bamnumericalindex_LDADD = ${LIBMAUS2LIBS}
bamnumericalindex_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamnumericalindex_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamnumericalindexstats_SOURCES = programs/bamnumericalindexstats.cpp biobambam2/Licensing.cpp
bamnumericalindexstats_LDADD = ${LIBMAUS2LIBS}
bamnumericalindexstats_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamnumericalindexstats_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammarkduplicatesopt_SOURCES = programs/bammarkduplicatesopt.cpp biobambam2/Licensing.cpp
bammarkduplicatesopt_LDADD = ${LIBMAUS2LIBS}
bammarkduplicatesopt_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammarkduplicatesopt_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bammarkduplicatesoptdist_SOURCES = programs/bammarkduplicatesoptdist.cpp biobambam2/Licensing.cpp
bammarkduplicatesoptdist_LDADD = ${LIBMAUS2LIBS}
bammarkduplicatesoptdist_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bammarkduplicatesoptdist_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterlength_SOURCES = programs/bamfilterlength.cpp biobambam2/Licensing.cpp
bamfilterlength_LDADD = ${LIBMAUS2LIBS}
bamfilterlength_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterlength_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamauxmerge_SOURCES = programs/bamauxmerge.cpp biobambam2/Licensing.cpp
bamauxmerge_LDADD = ${LIBMAUS2LIBS}
bamauxmerge_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamauxmerge_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamrefinterval_SOURCES = programs/bamrefinterval.cpp biobambam2/Licensing.cpp
bamrefinterval_LDADD = ${LIBMAUS2LIBS}
bamrefinterval_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamrefinterval_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamscrapcount_SOURCES = programs/bamscrapcount.cpp biobambam2/Licensing.cpp
bamscrapcount_LDADD = ${LIBMAUS2LIBS}
bamscrapcount_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamscrapcount_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterrefid_SOURCES = programs/bamfilterrefid.cpp biobambam2/Licensing.cpp
bamfilterrefid_LDADD = ${LIBMAUS2LIBS}
bamfilterrefid_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterrefid_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamrefextract_SOURCES = programs/bamrefextract.cpp biobambam2/Licensing.cpp
bamrefextract_LDADD = ${LIBMAUS2LIBS}
bamrefextract_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamrefextract_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bambisect_SOURCES = programs/bambisect.cpp biobambam2/Licensing.cpp
bambisect_LDADD = ${LIBMAUS2LIBS}
bambisect_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bambisect_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamreplacechecksums_SOURCES = programs/bamreplacechecksums.cpp biobambam2/Licensing.cpp biobambam2/ReadHeader.cpp biobambam2/RunEOFFilter.cpp biobambam2/UpdateNumericalIndex.cpp
bamreplacechecksums_LDADD = ${LIBMAUS2LIBS}
bamreplacechecksums_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamreplacechecksums_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcfconcat_SOURCES = programs/vcfconcat.cpp biobambam2/Licensing.cpp
vcfconcat_LDADD = ${LIBMAUS2LIBS}
vcfconcat_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcfconcat_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcfsort_SOURCES = programs/vcfsort.cpp biobambam2/Licensing.cpp
vcfsort_LDADD = ${LIBMAUS2LIBS}
vcfsort_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcfsort_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcfreplacecontigs_SOURCES = programs/vcfreplacecontigs.cpp biobambam2/Licensing.cpp
vcfreplacecontigs_LDADD = ${LIBMAUS2LIBS}
vcfreplacecontigs_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcfreplacecontigs_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfiltereofblocks_SOURCES = programs/bamfiltereofblocks.cpp biobambam2/Licensing.cpp biobambam2/RunEOFFilter.cpp
bamfiltereofblocks_LDADD = ${LIBMAUS2LIBS}
bamfiltereofblocks_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfiltereofblocks_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfeaturecount_SOURCES = programs/bamfeaturecount.cpp biobambam2/Licensing.cpp
bamfeaturecount_LDADD = ${LIBMAUS2LIBS}
bamfeaturecount_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfeaturecount_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfilterk_SOURCES = programs/bamfilterk.cpp biobambam2/Licensing.cpp biobambam2/DepthInterval.cpp biobambam2/RunEOFFilter.cpp
bamfilterk_LDADD = ${LIBMAUS2LIBS}
bamfilterk_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfilterk_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamfixpairinfo_SOURCES = programs/bamfixpairinfo.cpp biobambam2/Licensing.cpp biobambam2/DepthInterval.cpp biobambam2/RunEOFFilter.cpp
bamfixpairinfo_LDADD = ${LIBMAUS2LIBS}
bamfixpairinfo_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamfixpairinfo_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

filtergtf_SOURCES = programs/filtergtf.cpp biobambam2/Licensing.cpp
filtergtf_LDADD = ${LIBMAUS2LIBS}
filtergtf_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
filtergtf_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamexploderg_SOURCES = programs/bamexploderg.cpp biobambam2/Licensing.cpp
bamexploderg_LDADD = ${LIBMAUS2LIBS}
bamexploderg_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamexploderg_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamexondepth_SOURCES = programs/bamexondepth.cpp biobambam2/Licensing.cpp biobambam2/DepthInterval.cpp
bamexondepth_LDADD = ${LIBMAUS2LIBS}
bamexondepth_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamexondepth_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamheadercat_SOURCES = programs/bamheadercat.cpp biobambam2/Licensing.cpp
bamheadercat_LDADD = ${LIBMAUS2LIBS}
bamheadercat_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamheadercat_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

vcfdiff_SOURCES = programs/vcfdiff.cpp biobambam2/Licensing.cpp
vcfdiff_LDADD = ${LIBMAUS2LIBS}
vcfdiff_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
vcfdiff_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamdepthmerge_SOURCES = programs/bamdepthmerge.cpp biobambam2/Licensing.cpp
bamdepthmerge_LDADD = ${LIBMAUS2LIBS}
bamdepthmerge_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamdepthmerge_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}

bamcountflags_SOURCES = programs/bamcountflags.cpp biobambam2/Licensing.cpp
bamcountflags_LDADD = ${LIBMAUS2LIBS}
bamcountflags_LDFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS} ${LIBMAUS2LDFLAGS} ${AM_LDFLAGS}
bamcountflags_CPPFLAGS = ${AM_CPPFLAGS} ${LIBMAUS2CPPFLAGS}
